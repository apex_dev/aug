-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 21, 2017 at 12:03 PM
-- Server version: 5.7.13-0ubuntu0.16.04.2
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ddpc2`
--

-- --------------------------------------------------------

--
-- Table structure for table `banner_posts`
--

CREATE TABLE `banner_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `banner_post_header` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `banner_post_body` text COLLATE utf8_unicode_ci NOT NULL,
  `banner_post_images_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `banner_post_images_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_post_images_3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_post_images_4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_post_images_5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner_post_status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banner_posts`
--

INSERT INTO `banner_posts` (`id`, `banner_post_header`, `banner_post_body`, `banner_post_images_1`, `banner_post_images_2`, `banner_post_images_3`, `banner_post_images_4`, `banner_post_images_5`, `banner_post_status`, `created_at`, `updated_at`) VALUES
(1, '', '', 'IPP6tAaFt2.jpg', 'hODkNs2ibi.jpeg', 'CfwvIbBCUK.jpg', 'RFqkAlCgIR.jpg', 'NsTmkIy7ST.jpg', 0, '2017-09-20 03:15:58', '2017-09-20 03:15:58'),
(2, 'Blah', 'dsajlfl; jdsfal jfdls;kj fldsnf mvxvcjh uierhwekjfnmvliuxlas  m klnas;f ', 'QnDSzLXMHO.jpg', '2NqAe4ci25.jpeg', 'mgdDQgxddv.jpg', 'jQZ1ti7VgA.jpg', '1Fwdh4qow2.jpg', 0, '2017-09-20 03:16:21', '2017-09-20 03:16:21'),
(3, 'Blah', 'dsajlfl; jdsfal jfdls;kj fldsnf mvxvcjh uierhwekjfnmvliuxlas  m klnas;f ', 'iZ9yfGhow8.jpg', 'JCFlbpUtT8.jpeg', 'JmuUufPhyh.jpg', 'gysv0JOGSV.jpg', 'FxQPqmiZ1H.jpg', 0, '2017-09-20 03:16:33', '2017-09-20 03:16:33');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(1000) COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]',
  `color` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `tags`, `color`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Administration', '[]', '#000', NULL, '2017-08-24 03:59:36', '2017-08-24 03:59:36');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `designation` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Male',
  `mobile` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `mobile2` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dept` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_birth` date NOT NULL DEFAULT '1990-01-01',
  `date_hire` date NOT NULL,
  `date_left` date NOT NULL DEFAULT '1990-01-01',
  `salary_cur` decimal(15,3) NOT NULL DEFAULT '0.000',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `designation`, `gender`, `mobile`, `mobile2`, `email`, `dept`, `city`, `address`, `about`, `date_birth`, `date_hire`, `date_left`, `salary_cur`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Ko Min', 'Super Admin', 'Male', '8888888888', '', 'superadmin@gmail.com', 1, 'Yangon', 'Yangon, Myanmar', 'About user / biography', '2017-08-24', '2017-08-24', '2017-08-24', '300000.000', NULL, '2017-08-24 04:30:44', '2017-08-29 22:03:42');

-- --------------------------------------------------------

--
-- Table structure for table `la_configs`
--

CREATE TABLE `la_configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `la_configs`
--

INSERT INTO `la_configs` (`id`, `key`, `section`, `value`, `created_at`, `updated_at`) VALUES
(1, 'sitename', '', 'DDPC', '2017-08-24 03:59:40', '2017-09-05 22:47:45'),
(2, 'sitename_part1', '', 'Dawei Development', '2017-08-24 03:59:40', '2017-09-05 22:47:46'),
(3, 'sitename_part2', '', 'Public Company', '2017-08-24 03:59:40', '2017-09-05 22:47:46'),
(4, 'sitename_short', '', 'DD', '2017-08-24 03:59:40', '2017-09-05 22:47:46'),
(5, 'site_description', '', 'DDPC is a partnered company of APEX and Anawar Hlwan.', '2017-08-24 03:59:40', '2017-09-05 22:47:46'),
(6, 'sidebar_search', '', '0', '2017-08-24 03:59:40', '2017-09-05 22:47:46'),
(7, 'show_messages', '', '0', '2017-08-24 03:59:40', '2017-09-05 22:47:46'),
(8, 'show_notifications', '', '0', '2017-08-24 03:59:40', '2017-09-05 22:47:46'),
(9, 'show_tasks', '', '0', '2017-08-24 03:59:40', '2017-09-05 22:47:46'),
(10, 'show_rightsidebar', '', '0', '2017-08-24 03:59:40', '2017-09-05 22:47:46'),
(11, 'skin', '', 'skin-black', '2017-08-24 03:59:40', '2017-09-05 22:47:46'),
(12, 'layout', '', 'fixed', '2017-08-24 03:59:40', '2017-09-05 22:47:46'),
(13, 'default_email', '', 'ddpc_admin@gmail.com', '2017-08-24 03:59:40', '2017-09-05 22:47:46');

-- --------------------------------------------------------

--
-- Table structure for table `la_menus`
--

CREATE TABLE `la_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'module',
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hierarchy` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `la_menus`
--

INSERT INTO `la_menus` (`id`, `name`, `url`, `icon`, `type`, `parent`, `hierarchy`, `created_at`, `updated_at`) VALUES
(12, 'Users', 'users', 'fa-group', 'module', 11, 1, '2017-08-24 22:19:06', '2017-08-24 22:19:09');

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_menu_status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `main_category_tag` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `category_name`, `category_menu_status`, `created_at`, `updated_at`, `main_category_tag`) VALUES
(6, 'Education', NULL, '2017-09-15 22:45:17', '2017-09-17 20:50:27', 'education'),
(7, 'Shopping Center', NULL, '2017-09-15 22:45:29', '2017-09-17 20:50:53', 'shopping_center'),
(8, 'Wedding Hall', NULL, '2017-09-15 22:45:41', '2017-09-17 20:51:01', 'wedding_hall'),
(9, 'Petrol Stations', NULL, '2017-09-15 22:45:57', '2017-09-17 20:51:15', 'petrol_stations'),
(10, 'Power Distribution', NULL, '2017-09-15 22:46:25', '2017-09-17 20:51:35', 'power_distribution'),
(11, 'Project', NULL, '2017-09-15 22:46:36', '2017-09-17 20:51:42', 'project'),
(12, 'Hotel', NULL, '2017-09-15 22:46:45', '2017-09-17 20:51:48', 'hotel'),
(13, 'General', 1, '2017-09-17 20:36:37', '2017-09-17 20:51:55', 'general');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_05_26_050000_create_modules_table', 1),
('2014_05_26_055000_create_module_field_types_table', 1),
('2014_05_26_060000_create_module_fields_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2014_12_01_000000_create_uploads_table', 1),
('2016_05_26_064006_create_departments_table', 1),
('2016_05_26_064007_create_employees_table', 1),
('2016_05_26_064446_create_roles_table', 1),
('2016_07_05_115343_create_role_user_table', 1),
('2016_07_06_140637_create_organizations_table', 1),
('2016_07_07_134058_create_backups_table', 1),
('2016_07_07_134058_create_menus_table', 1),
('2016_09_10_163337_create_permissions_table', 1),
('2016_09_10_163520_create_permission_role_table', 1),
('2016_09_22_105958_role_module_fields_table', 1),
('2016_09_22_110008_role_module_table', 1),
('2016_10_06_115413_create_la_configs_table', 1),
('2017_08_25_080421_create_posts_table', 2),
('2017_09_06_103012_create_posts_category_table', 3),
('2017_09_08_042949_create_sub_category_table', 4),
('2017_09_18_025136_tag_field_added_in_category_table', 5),
('2017_09_18_025339_tag_field_added_in_sub_category_table', 5),
('2017_09_20_072921_add_featureimage_field_to_post_table', 6),
('2017_09_20_023613_create_banner_post_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_db` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `view_col` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fa_icon` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'fa-cube',
  `is_gen` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `label`, `name_db`, `view_col`, `model`, `controller`, `fa_icon`, `is_gen`, `created_at`, `updated_at`) VALUES
(1, 'Users', 'Users', 'users', 'name', 'User', 'UsersController', 'fa-group', 1, '2017-08-24 03:59:08', '2017-08-24 03:59:40'),
(2, 'Uploads', 'Uploads', 'uploads', 'name', 'Upload', 'UploadsController', 'fa-files-o', 1, '2017-08-24 03:59:10', '2017-08-24 03:59:40'),
(3, 'Departments', 'Departments', 'departments', 'name', 'Department', 'DepartmentsController', 'fa-tags', 1, '2017-08-24 03:59:12', '2017-08-24 03:59:40'),
(4, 'Employees', 'Employees', 'employees', 'name', 'Employee', 'EmployeesController', 'fa-group', 1, '2017-08-24 03:59:13', '2017-08-24 03:59:40'),
(5, 'Roles', 'Roles', 'roles', 'name', 'Role', 'RolesController', 'fa-user-plus', 1, '2017-08-24 03:59:15', '2017-08-24 03:59:41'),
(8, 'Permissions', 'Permissions', 'permissions', 'name', 'Permission', 'PermissionsController', 'fa-magic', 1, '2017-08-24 03:59:26', '2017-08-24 03:59:41');

-- --------------------------------------------------------

--
-- Table structure for table `module_fields`
--

CREATE TABLE `module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `colname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module` int(10) UNSIGNED NOT NULL,
  `field_type` int(10) UNSIGNED NOT NULL,
  `unique` tinyint(1) NOT NULL DEFAULT '0',
  `defaultvalue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minlength` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `maxlength` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `popup_vals` text COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_fields`
--

INSERT INTO `module_fields` (`id`, `colname`, `label`, `module`, `field_type`, `unique`, `defaultvalue`, `minlength`, `maxlength`, `required`, `popup_vals`, `sort`, `created_at`, `updated_at`) VALUES
(1, 'name', 'Name', 1, 16, 0, '', 5, 250, 1, '', 0, '2017-08-24 03:59:08', '2017-08-24 03:59:08'),
(2, 'context_id', 'Context', 1, 13, 0, '0', 0, 0, 0, '', 0, '2017-08-24 03:59:08', '2017-08-24 03:59:08'),
(3, 'email', 'Email', 1, 8, 1, '', 0, 250, 0, '', 0, '2017-08-24 03:59:08', '2017-08-24 03:59:08'),
(4, 'password', 'Password', 1, 17, 0, '', 6, 250, 1, '', 0, '2017-08-24 03:59:08', '2017-08-24 03:59:08'),
(5, 'type', 'User Type', 1, 7, 0, 'Employee', 0, 0, 0, '["Employee","Client"]', 0, '2017-08-24 03:59:08', '2017-08-24 03:59:08'),
(6, 'name', 'Name', 2, 16, 0, '', 5, 250, 1, '', 0, '2017-08-24 03:59:10', '2017-08-24 03:59:10'),
(7, 'path', 'Path', 2, 19, 0, '', 0, 250, 0, '', 0, '2017-08-24 03:59:10', '2017-08-24 03:59:10'),
(8, 'extension', 'Extension', 2, 19, 0, '', 0, 20, 0, '', 0, '2017-08-24 03:59:10', '2017-08-24 03:59:10'),
(9, 'caption', 'Caption', 2, 19, 0, '', 0, 250, 0, '', 0, '2017-08-24 03:59:10', '2017-08-24 03:59:10'),
(10, 'user_id', 'Owner', 2, 7, 0, '1', 0, 0, 0, '@users', 0, '2017-08-24 03:59:10', '2017-08-24 03:59:10'),
(11, 'hash', 'Hash', 2, 19, 0, '', 0, 250, 0, '', 0, '2017-08-24 03:59:10', '2017-08-24 03:59:10'),
(12, 'public', 'Is Public', 2, 2, 0, '0', 0, 0, 0, '', 0, '2017-08-24 03:59:11', '2017-08-24 03:59:11'),
(13, 'name', 'Name', 3, 16, 1, '', 1, 250, 1, '', 0, '2017-08-24 03:59:12', '2017-08-24 03:59:12'),
(14, 'tags', 'Tags', 3, 20, 0, '[]', 0, 0, 0, '', 0, '2017-08-24 03:59:12', '2017-08-24 03:59:12'),
(15, 'color', 'Color', 3, 19, 0, '', 0, 50, 1, '', 0, '2017-08-24 03:59:12', '2017-08-24 03:59:12'),
(16, 'name', 'Name', 4, 16, 0, '', 5, 250, 1, '', 0, '2017-08-24 03:59:13', '2017-08-24 03:59:13'),
(17, 'designation', 'Designation', 4, 19, 0, '', 0, 50, 1, '', 0, '2017-08-24 03:59:13', '2017-08-24 03:59:13'),
(18, 'gender', 'Gender', 4, 18, 0, 'Male', 0, 0, 1, '["Male","Female"]', 0, '2017-08-24 03:59:13', '2017-08-24 03:59:13'),
(19, 'mobile', 'Mobile', 4, 14, 0, '', 10, 20, 1, '', 0, '2017-08-24 03:59:13', '2017-08-24 03:59:13'),
(20, 'mobile2', 'Alternative Mobile', 4, 14, 0, '', 10, 20, 0, '', 0, '2017-08-24 03:59:13', '2017-08-24 03:59:13'),
(21, 'email', 'Email', 4, 8, 1, '', 5, 250, 1, '', 0, '2017-08-24 03:59:13', '2017-08-24 03:59:13'),
(22, 'dept', 'Department', 4, 7, 0, '0', 0, 0, 1, '@departments', 0, '2017-08-24 03:59:13', '2017-08-24 03:59:13'),
(23, 'city', 'City', 4, 19, 0, '', 0, 50, 0, '', 0, '2017-08-24 03:59:13', '2017-08-24 03:59:13'),
(24, 'address', 'Address', 4, 1, 0, '', 0, 1000, 0, '', 0, '2017-08-24 03:59:13', '2017-08-24 03:59:13'),
(25, 'about', 'About', 4, 19, 0, '', 0, 0, 0, '', 0, '2017-08-24 03:59:13', '2017-08-24 03:59:13'),
(26, 'date_birth', 'Date of Birth', 4, 4, 0, '1990-01-01', 0, 0, 0, '', 0, '2017-08-24 03:59:13', '2017-08-24 03:59:13'),
(27, 'date_hire', 'Hiring Date', 4, 4, 0, 'date(\'Y-m-d\')', 0, 0, 0, '', 0, '2017-08-24 03:59:13', '2017-08-24 03:59:13'),
(28, 'date_left', 'Resignation Date', 4, 4, 0, '1990-01-01', 0, 0, 0, '', 0, '2017-08-24 03:59:14', '2017-08-24 03:59:14'),
(29, 'salary_cur', 'Current Salary', 4, 6, 0, '0.0', 0, 2, 0, '', 0, '2017-08-24 03:59:14', '2017-08-24 03:59:14'),
(30, 'name', 'Name', 5, 16, 1, '', 1, 250, 1, '', 0, '2017-08-24 03:59:15', '2017-08-24 03:59:15'),
(31, 'display_name', 'Display Name', 5, 19, 0, '', 0, 250, 1, '', 0, '2017-08-24 03:59:15', '2017-08-24 03:59:15'),
(32, 'description', 'Description', 5, 21, 0, '', 0, 1000, 0, '', 0, '2017-08-24 03:59:15', '2017-08-24 03:59:15'),
(33, 'parent', 'Parent Role', 5, 7, 0, '1', 0, 0, 0, '@roles', 0, '2017-08-24 03:59:15', '2017-08-24 03:59:15'),
(34, 'dept', 'Department', 5, 7, 0, '1', 0, 0, 0, '@departments', 0, '2017-08-24 03:59:15', '2017-08-24 03:59:15'),
(49, 'name', 'Name', 8, 16, 1, '', 1, 250, 1, '', 0, '2017-08-24 03:59:26', '2017-08-24 03:59:26'),
(50, 'display_name', 'Display Name', 8, 19, 0, '', 0, 250, 1, '', 0, '2017-08-24 03:59:26', '2017-08-24 03:59:26'),
(51, 'description', 'Description', 8, 21, 0, '', 0, 1000, 0, '', 0, '2017-08-24 03:59:26', '2017-08-24 03:59:26');

-- --------------------------------------------------------

--
-- Table structure for table `module_field_types`
--

CREATE TABLE `module_field_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `module_field_types`
--

INSERT INTO `module_field_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Address', '2017-08-24 03:59:04', '2017-08-24 03:59:04'),
(2, 'Checkbox', '2017-08-24 03:59:04', '2017-08-24 03:59:04'),
(3, 'Currency', '2017-08-24 03:59:04', '2017-08-24 03:59:04'),
(4, 'Date', '2017-08-24 03:59:04', '2017-08-24 03:59:04'),
(5, 'Datetime', '2017-08-24 03:59:04', '2017-08-24 03:59:04'),
(6, 'Decimal', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(7, 'Dropdown', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(8, 'Email', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(9, 'File', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(10, 'Float', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(11, 'HTML', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(12, 'Image', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(13, 'Integer', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(14, 'Mobile', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(15, 'Multiselect', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(16, 'Name', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(17, 'Password', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(18, 'Radio', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(19, 'String', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(20, 'Taginput', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(21, 'Textarea', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(22, 'TextField', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(23, 'URL', '2017-08-24 03:59:05', '2017-08-24 03:59:05'),
(24, 'Files', '2017-08-24 03:59:05', '2017-08-24 03:59:05');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN_PANEL', 'Admin Panel', 'Admin Panel Permission', NULL, '2017-08-24 03:59:39', '2017-08-24 03:59:39');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_category_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_header` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_body_detail` text COLLATE utf8_unicode_ci NOT NULL,
  `feature_post_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `feature_post_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `sub_category_id`, `post_header`, `post_body_detail`, `feature_post_status`, `post_image`, `created_at`, `updated_at`, `feature_post_image`) VALUES
(1, '20', 'Edu Park', '<p><span style="font-family: Zawgyi-One,serif;">EduPark ( Human Resource Development Center) </span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">သည္ ႏိုင္ငံတကာႏွင့္ ရင္ေဘာင္တန္းႏိုင္ေသာ မ်ိဳးဆက္သစ္ လူငယ္ မ်ားေဒသ အတြင္းေပၚထြက္လာေစရန္ အေျခခံပညာမွစ၍ ဘက္စံုဖြံြ႕ၿဖိဳးေရး စီမံခ်က္အား </span></span><span style="font-family: Zawgyi-One,serif;">(</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၂၀၁၄ </span></span><span style="font-family: Zawgyi-One,serif;">- </span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၂၀၁၅ </span></span><span style="font-family: Zawgyi-One,serif;">) </span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ပညာသင္ ႏွစ္မွ စတင္၍ </span></span><span style="font-family: Zawgyi-One,serif;">Pilot Project </span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">အေနျဖင့္ </span></span><span style="font-family: Zawgyi-One,serif;">HRD Centre </span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၌ စတင္ အေကာင္အထည္ေဖာ္ ေဆာင္ရြက္ခဲ့ပါသည္။ </span></span><span style="font-family: Zawgyi-One,serif;">(</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၂၀၁၅ </span></span><span style="font-family: Zawgyi-One,serif;">- </span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၂၀၁၆</span></span><span style="font-family: Zawgyi-One,serif;">) </span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ပညာသင္ႏွစ္မွာပင္ တကၠသိုလ္ ၀င္တန္းအား </span></span><span style="font-family: Zawgyi-One,serif;">(</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၄</span></span><span style="font-family: Zawgyi-One,serif;">)</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ဘာသာ ဂုဏ္ထူးရွင္ အပါအ၀င္ ဂုဏ္ထူးျဖင့္ ေအာင္ျမင္သူ </span></span><span style="font-family: Zawgyi-One,serif;">(</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၇</span></span><span style="font-family: Zawgyi-One,serif;">)</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ဦး ေမြးထုတ္ႏုိင္ခဲ့ၿပီး စာေမးပြဲေအာင္ခ်က္မွာ ထား၀ယ္ခရိုင္တြင္ ပထမ ၊ တနသၤာရီတိုင္း ေဒသႀကီးတြင္ ဒုတိယ အေနျဖင့္ ရပ္တည္ႏိုင္ခဲ့ပါသည္။ လာမည့္ႏွစ္ </span></span><span style="font-family: Zawgyi-One,serif;">(</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၂၀၁၆ </span></span><span style="font-family: Zawgyi-One,serif;">-</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၂၀၁၇</span></span><span style="font-family: Zawgyi-One,serif;">)</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ပညာသင္ႏွစ္တြင္ တကၠသိုလ္၀င္တန္း </span></span><span style="font-family: Zawgyi-One,serif;">( </span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၃၂ </span></span><span style="font-family: Zawgyi-One,serif;">)</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ဦး ျဖင့္ ၀င္ေရာက္ေျဖဆိုခဲ့ပါသည္။ ၂၀၁၆ </span></span><span style="font-family: Zawgyi-One,serif;">-</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၂၀၁၇</span></span><span style="font-family: Zawgyi-One,serif;">)</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ပညာသင္ႏွစ္ တြင္ </span></span><span style="font-family: Zawgyi-One,serif;">(</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၄</span></span><span style="font-family: Zawgyi-One,serif;">)</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ဘာသာ ဂုဏ္ထူးရွင္ အပါအ၀င္ ဂုဏ္ထူးျဖင့္ေအာင္ျမင္သူ </span></span><span style="font-family: Zawgyi-One,serif;">(</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၇</span></span><span style="font-family: Zawgyi-One,serif;">)</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ဦး ေမြးထုတ္ ႏိုင္ခဲ့ၿပီး စာေမးပြဲေအာင္ခ်က္မွာ ထား၀ယ္ခရိုင္တြင္ ပထမေအာင္ခ်က္ အေကာင္းဆံုး ေက်ာင္းအျဖစ္ ရပ္တည္ႏိုင္ခဲ့ပါသည္။</span></span></p>', '1', 'zi6Cb660ut.jpg', '2017-09-12 22:05:22', '2017-09-20 04:20:47', 'ZZuwN4eOQ8.jpg'),
(2, '20', 'About Banana', '<p>EduPark ( Human&nbsp; Resource&nbsp; Development Center) သည္ ႏိုင္ငံတကာႏွင့္ ရင္ေဘာင္တန္းႏိုင္ေသာ&nbsp; မ်ိဳးဆက္သစ္ လူငယ္ မ်ားေဒသ အတြင္းေပၚထြက္လာေစရန္ အေျခခံပညာမွစ၍ ဘက္စံုဖြံြ႕ၿဖိဳးေရး စီမံခ်က္အား (၂၀၁၄ - ၂၀၁၅ ) ပညာသင္ ႏွစ္မွ စတင္၍&nbsp;&nbsp; Pilot&nbsp;&nbsp; Project&nbsp;&nbsp; အေနျဖင့္&nbsp; HRD&nbsp; Centre&nbsp; ၌ စတင္ အေကာင္အထည္ေဖာ္ ေဆာင္ရြက္ခဲ့ပါသည္။ (၂၀၁၅ - ၂၀၁၆) ပညာသင္ႏွစ္မွာပင္ တကၠသိုလ္ ၀င္တန္းအား (၄)ဘာသာ ဂုဏ္ထူးရွင္&nbsp; အပါအ၀င္&nbsp; ဂုဏ္ထူးျဖင့္ ေအာင္ျမင္သူ (၇)ဦး ေမြးထုတ္ႏုိင္ခဲ့ၿပီး စာေမးပြဲေအာင္ခ်က္မွာ&nbsp;&nbsp;&nbsp; ထား၀ယ္ခရိုင္တြင္&nbsp;&nbsp; ပထမ ၊&nbsp; တနသၤာရီတိုင္း ေဒသႀကီးတြင္ ဒုတိယ အေနျဖင့္ ရပ္တည္ႏိုင္ခဲ့ပါသည္။ လာမည့္ႏွစ္ (၂၀၁၆ -၂၀၁၇)ပညာသင္ႏွစ္တြင္ တကၠသိုလ္၀င္တန္း ( ၃၂ )ဦး ျဖင့္ ၀င္ေရာက္ေျဖဆိုခဲ့ပါသည္။ ၂၀၁၆ -၂၀၁၇)ပညာသင္ႏွစ္ တြင္ (၄)ဘာသာ ဂုဏ္ထူးရွင္ အပါအ၀င္ ဂုဏ္ထူးျဖင့္ေအာင္ျမင္သူ (၇)ဦး ေမြးထုတ္ ႏိုင္ခဲ့ၿပီး စာေမးပြဲေအာင္ခ်က္မွာ ထား၀ယ္ခရိုင္တြင္ ပထမေအာင္ခ်က္ အေကာင္းဆံုး ေက်ာင္းအျဖစ္ ရပ္တည္ႏိုင္ခဲ့ပါသည္။</p>', '0', 'dM6EQiVF6X.jpg', '2017-09-15 00:24:26', '2017-09-20 04:25:27', 'Eq9qHr66hQ.jpg'),
(3, '20', 'Where can I get some?', '<p>&nbsp;There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', '1', 'JvL75qWWkQ.jpg', '2017-09-17 21:16:45', '2017-09-17 22:20:24', ''),
(4, '6', 'Supermarket', '<p><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ထား၀ယ္ေဒသ၏တုိးတက္ေျပာင္းလဲ လာမည့္ စီးပြားေရး၊ လူမႈေရးႏွင့္ ေဒသခံျပည္သူတုိ႔၏ လူေနမႈ အဆင့္အတန္းတုိးတက္ျမင့္မားလာေစရန္ႏွင့္ တစ္ေနရာတည္းတြင္ လူသံုးကုန္ပစၥည္းအမ်ိဳးမ်ိဳး၊ လွ်ပ္စစ္ပစၥည္းအမ်ိဳးမ်ိဳး၊ အလွကုန္ ႏွင့္ အျခားေသာ ပစၥည္းမ်ားကို ေစ်းႏွဳန္းခ်ိဳသာစြာႏွင့္ စံုလင္စြာရရွိႏိုင္ရန္ </span></span><span style="font-family: Zawgyi-One,serif;">DDPC Supermarket </span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ကို၂၀၁၆ ခုႏွစ္ ေအာက္တိုဘာလ </span></span><span style="font-family: Zawgyi-One,serif;">(</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၉</span></span><span style="font-family: Zawgyi-One,serif;">) </span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ရက္ေန႕တြင္ ခမ္းနားထည္၀ါစြာဖြင့္လွစ္ ႏိုင္ခဲ့ပါသည္။</span></span></p>', '1', 'vgggf3Kdti.jpg', '2017-09-18 01:31:56', '2017-09-19 21:14:09', ''),
(5, '7', 'Royal Coffee', '<p><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ထား၀ယ္ေဒသ၏တုိးတက္ေျပာင္းလဲ လာမည့္ စီးပြားေရး၊ လူမႈေရးႏွင့္ ေဒသခံျပည္သူတုိ႔၏ လူေနမႈ အဆင့္အတန္းတုိးတက္ျမင့္မားလာေစရန္ႏွင့္ တစ္ေနရာတည္းတြင္ လူသံုးကုန္ပစၥည္းအမ်ိဳးမ်ိဳး၊ လွ်ပ္စစ္ပစၥည္းအမ်ိဳးမ်ိဳး၊ အလွကုန္ ႏွင့္ အျခားေသာ ပစၥည္းမ်ားကို ေစ်းႏွဳန္းခ်ိဳသာစြာႏွင့္ စံုလင္စြာရရွိႏိုင္ရန္ </span></span><span style="font-family: Zawgyi-One,serif;">DDPC Supermarket </span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ကို၂၀၁၆ ခုႏွစ္ ေအာက္တိုဘာလ </span></span><span style="font-family: Zawgyi-One,serif;">(</span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">၉</span></span><span style="font-family: Zawgyi-One,serif;">) </span><span lang="ar-SA"><span style="font-family: Zawgyi-One;">ရက္ေန႕တြင္ ခမ္းနားထည္၀ါစြာဖြင့္လွစ္ ႏိုင္ခဲ့ပါသည္။</span></span></p>', '', '7SnlKuifBm.jpg', '2017-09-18 01:46:53', '2017-09-19 21:14:44', ''),
(6, '17', 'sdlkafjdskfjlk  alksdjf dsaflkj  sdfsaldj sdfa a်ိျေ် ျိ်  ', '<p>fgsadgdfsgfdsgdfsgsdfhtyjkjk&nbsp; dgs sdfg df gda fgadgdfgf ag a gfdgasdgaga ggaagfdg a ag fg</p>', '', 'gwGeqjbqYx.pdf', '2017-09-18 02:19:09', '2017-09-19 04:14:11', ''),
(7, '20', 'Testing 1', '<p>sdafsaf d fas fdsa fsadf sdaf asd fsad fsadf sdaf</p>', '', 'W0v3rHPBNk.jpg', '2017-09-20 01:16:59', '2017-09-20 01:45:23', '');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `dept` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `parent`, `dept`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'SUPER_ADMIN', 'Super Admin', 'Full Access Role', 1, 1, NULL, '2017-08-24 03:59:36', '2017-08-24 03:59:36');

-- --------------------------------------------------------

--
-- Table structure for table `role_module`
--

CREATE TABLE `role_module` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `acc_view` tinyint(1) NOT NULL,
  `acc_create` tinyint(1) NOT NULL,
  `acc_edit` tinyint(1) NOT NULL,
  `acc_delete` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_module`
--

INSERT INTO `role_module` (`id`, `role_id`, `module_id`, `acc_view`, `acc_create`, `acc_edit`, `acc_delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, 1, 1, '2017-08-24 03:59:36', '2017-08-24 03:59:36'),
(2, 1, 2, 1, 1, 1, 1, '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(3, 1, 3, 1, 1, 1, 1, '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(4, 1, 4, 1, 1, 1, 1, '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(5, 1, 5, 1, 1, 1, 1, '2017-08-24 03:59:38', '2017-08-24 03:59:38'),
(8, 1, 8, 1, 1, 1, 1, '2017-08-24 03:59:39', '2017-08-24 03:59:39');

-- --------------------------------------------------------

--
-- Table structure for table `role_module_fields`
--

CREATE TABLE `role_module_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `field_id` int(10) UNSIGNED NOT NULL,
  `access` enum('invisible','readonly','write') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_module_fields`
--

INSERT INTO `role_module_fields` (`id`, `role_id`, `field_id`, `access`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'write', '2017-08-24 03:59:36', '2017-08-24 03:59:36'),
(2, 1, 2, 'write', '2017-08-24 03:59:36', '2017-08-24 03:59:36'),
(3, 1, 3, 'write', '2017-08-24 03:59:36', '2017-08-24 03:59:36'),
(4, 1, 4, 'write', '2017-08-24 03:59:36', '2017-08-24 03:59:36'),
(5, 1, 5, 'write', '2017-08-24 03:59:36', '2017-08-24 03:59:36'),
(6, 1, 6, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(7, 1, 7, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(8, 1, 8, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(9, 1, 9, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(10, 1, 10, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(11, 1, 11, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(12, 1, 12, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(13, 1, 13, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(14, 1, 14, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(15, 1, 15, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(16, 1, 16, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(17, 1, 17, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(18, 1, 18, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(19, 1, 19, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(20, 1, 20, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(21, 1, 21, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(22, 1, 22, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(23, 1, 23, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(24, 1, 24, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(25, 1, 25, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(26, 1, 26, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(27, 1, 27, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(28, 1, 28, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(29, 1, 29, 'write', '2017-08-24 03:59:37', '2017-08-24 03:59:37'),
(30, 1, 30, 'write', '2017-08-24 03:59:38', '2017-08-24 03:59:38'),
(31, 1, 31, 'write', '2017-08-24 03:59:38', '2017-08-24 03:59:38'),
(32, 1, 32, 'write', '2017-08-24 03:59:38', '2017-08-24 03:59:38'),
(33, 1, 33, 'write', '2017-08-24 03:59:38', '2017-08-24 03:59:38'),
(34, 1, 34, 'write', '2017-08-24 03:59:38', '2017-08-24 03:59:38'),
(49, 1, 49, 'write', '2017-08-24 03:59:39', '2017-08-24 03:59:39'),
(50, 1, 50, 'write', '2017-08-24 03:59:39', '2017-08-24 03:59:39'),
(51, 1, 51, 'write', '2017-08-24 03:59:39', '2017-08-24 03:59:39');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `main_category_id` int(10) UNSIGNED NOT NULL,
  `sub_category_menu_status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sub_category_tag` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `sub_category_name`, `main_category_id`, `sub_category_menu_status`, `created_at`, `updated_at`, `sub_category_tag`) VALUES
(3, 'EduPark Private School', 6, NULL, '2017-09-15 22:48:03', '2017-09-17 22:32:55', 'edupark_private_school'),
(4, 'Private Pre-School', 6, NULL, '2017-09-15 22:48:27', '2017-09-17 22:33:13', 'private_pre_school'),
(5, 'SEO', 6, NULL, '2017-09-15 22:48:45', '2017-09-17 22:33:20', 'seo'),
(6, 'DDPC Supermarket', 7, NULL, '2017-09-15 22:49:21', '2017-09-17 22:33:31', 'ddpc_supermarket'),
(7, 'Royal Coffee', 7, NULL, '2017-09-15 22:49:49', '2017-09-17 22:33:42', 'royal_coffee'),
(8, 'D Fashion', 7, NULL, '2017-09-15 22:50:52', '2017-09-17 22:33:51', 'd_fashion'),
(9, 'DDPC Food Center', 7, NULL, '2017-09-15 22:51:02', '2017-09-17 22:34:08', 'ddpc_food_center'),
(10, 'Other Shop', 7, NULL, '2017-09-15 22:51:20', '2017-09-17 22:34:17', 'other_shop'),
(11, 'Royal Palace', 8, NULL, '2017-09-15 22:51:56', '2017-09-17 22:34:29', 'royal_palace'),
(12, 'Grass Wedding', 8, NULL, '2017-09-15 22:52:17', '2017-09-17 22:34:42', 'grass_wedding'),
(13, 'DDPC Petrol Station', 9, NULL, '2017-09-15 22:52:40', '2017-09-17 22:34:57', 'ddpc_petrol_station'),
(14, 'Shophouse', 11, NULL, '2017-09-15 22:54:02', '2017-09-17 22:35:07', 'shophouse'),
(15, 'Tarraced House', 11, NULL, '2017-09-15 22:54:13', '2017-09-17 22:35:18', 'tarraced_house'),
(16, 'Dawei Htee Khee Bus Terminal', 11, NULL, '2017-09-15 22:55:53', '2017-09-17 22:35:37', 'dawei_htee_khee_bus_terminal'),
(17, 'DDPC Hotels', 12, NULL, '2017-09-15 22:56:24', '2017-09-17 22:35:48', 'ddpc_hotels'),
(18, 'Orchid Residential Project', 12, NULL, '2017-09-15 22:56:40', '2017-09-17 22:36:04', 'orchid_residential_project'),
(19, 'Power Distribution', 10, NULL, '2017-09-15 23:01:51', '2017-09-17 22:36:16', 'power_distribution'),
(20, 'General', 13, NULL, '2017-09-17 20:36:50', '2017-09-17 22:36:24', 'general');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE `uploads` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hash` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `name`, `path`, `extension`, `caption`, `user_id`, `hash`, `public`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Carcapacityfiilldata.JPG', '/var/www/html/DDPC/storage/uploads/2017-08-29-075356-Carcapacityfiilldata.JPG', 'JPG', '', 1, 'qnzbgelhsp1dnrwojkuh', 1, '2017-08-29 02:19:31', '2017-08-29 01:23:56', '2017-08-29 02:19:31'),
(2, 'Tournament Prediction.jpg', '/var/www/html/DDPC/storage/uploads/2017-08-29-084938-Tournament Prediction.jpg', 'jpg', '', 1, 'mwr5zxdlsk12sryewysp', 0, '2017-08-30 21:14:31', '2017-08-29 02:19:39', '2017-08-30 21:14:31'),
(3, 'Team Prediction.jpg', '/var/www/html/DDPC/storage/uploads/2017-08-29-084942-Team Prediction.jpg', 'jpg', '', 1, 'lf46knrc1dzkvwbwwmb6', 0, '2017-08-30 21:14:34', '2017-08-29 02:19:42', '2017-08-30 21:14:34'),
(4, 'stock-vector-myanmar-boy-and-girl-in-traditional-costume-240777370.jpg', '/var/www/html/DDPC/storage/uploads/2017-08-30-092528-stock-vector-myanmar-boy-and-girl-in-traditional-costume-240777370.jpg', 'jpg', '', 1, 'tb3x5rndjxpyd1wjh8bm', 0, '2017-08-30 21:14:37', '2017-08-30 02:55:28', '2017-08-30 21:14:37'),
(5, 'Screenshot from 2017-07-11 16-00-24.png', '/var/www/html/DDPC/storage/uploads/2017-08-31-033104-Screenshot from 2017-07-11 16-00-24.png', 'png', '', 1, 'jxhini6kkbrkzb8xortp', 0, '2017-08-30 21:14:43', '2017-08-30 21:01:04', '2017-08-30 21:14:43'),
(6, '20.jpg', 'storage/uploads/2017-08-31-034020-20.jpg', 'jpg', '', 1, 'iuatcy25q184vihhavlt', 0, '2017-08-30 21:14:40', '2017-08-30 21:10:20', '2017-08-30 21:14:40'),
(7, 'images.jpeg', '/var/www/html/DDPC/public/uploads/2017-08-31-034307-images.jpeg', 'jpeg', '', 1, 'ijrlrujqmk0kvdgr1e5o', 0, '2017-09-04 01:28:04', '2017-08-30 21:13:07', '2017-09-04 01:28:04'),
(8, 'no-image.png', '/var/www/html/DDPC/public/uploads/2017-08-31-034502-no-image.png', 'png', '', 1, 'ldteabq9zjnfr1gktg2v', 0, '2017-09-04 01:28:07', '2017-08-30 21:15:02', '2017-09-04 01:28:07'),
(9, 'Team Prediction.jpg', '/var/www/html/DDPC/public/uploads/2017-09-04-051434-Team Prediction.jpg', 'jpg', '', 1, '7czvvbecsjcizrh1hnmq', 0, '2017-09-04 01:28:10', '2017-09-03 22:44:34', '2017-09-04 01:28:10'),
(10, 'stock-vector-asean-boys-and-girls-in-traditional-costume-with-flag-178822796.jpg', 'public/uploads/2017-09-04-052024-stock-vector-asean-boys-and-girls-in-traditional-costume-with-flag-178822796.jpg', 'jpg', '', 1, '', 0, '2017-09-04 01:28:13', '2017-09-03 22:50:24', '2017-09-04 01:28:13'),
(11, 'viewFile.png', 'public/uploads/2017-09-04-075546-viewFile.png', 'png', '', 1, '', 0, NULL, '2017-09-04 01:25:46', '2017-09-04 01:25:46'),
(12, 'stock-vector-myanmar-boy-and-girl-in-traditional-costume-240777370.jpg', 'uploads/2017-09-04-082150-stock-vector-myanmar-boy-and-girl-in-traditional-costume-240777370.jpg', 'jpg', '', 1, '', 0, NULL, '2017-09-04 01:51:50', '2017-09-04 01:51:50'),
(13, 'no-image-logo.png', 'uploads/2017-09-04-082215-no-image-logo.png', 'png', '', 1, '', 0, NULL, '2017-09-04 01:52:15', '2017-09-04 01:52:15'),
(14, '19.jpg', 'uploads/2017-09-04-083554-19.jpg', 'jpg', '', 1, '', 0, NULL, '2017-09-04 02:05:54', '2017-09-04 02:05:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `context_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Employee',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `context_id`, `email`, `password`, `type`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Ko Min', 1, 'superadmin@gmail.com', '$2y$10$Tc32YYlh8pTnnCuS5eABN.ikTgXd2NS4rBZTMMWVgl3Iv3WX0PjRK', 'Employee', 'tdH40LVVaCrSLiR1UZJLyX6HBge7FJUy9UYR1pTJ0SU6OPLAkZx8lBLHPa09', NULL, '2017-08-24 04:30:44', '2017-09-18 02:55:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banner_posts`
--
ALTER TABLE `banner_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_name_unique` (`name`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_email_unique` (`email`),
  ADD KEY `employees_dept_foreign` (`dept`);

--
-- Indexes for table `la_configs`
--
ALTER TABLE `la_configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `la_menus`
--
ALTER TABLE `la_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_fields`
--
ALTER TABLE `module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_fields_module_foreign` (`module`),
  ADD KEY `module_fields_field_type_foreign` (`field_type`);

--
-- Indexes for table `module_field_types`
--
ALTER TABLE `module_field_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`),
  ADD KEY `roles_parent_foreign` (`parent`),
  ADD KEY `roles_dept_foreign` (`dept`);

--
-- Indexes for table `role_module`
--
ALTER TABLE `role_module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_role_id_foreign` (`role_id`),
  ADD KEY `role_module_module_id_foreign` (`module_id`);

--
-- Indexes for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_module_fields_role_id_foreign` (`role_id`),
  ADD KEY `role_module_fields_field_id_foreign` (`field_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploads_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banner_posts`
--
ALTER TABLE `banner_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `la_configs`
--
ALTER TABLE `la_configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `la_menus`
--
ALTER TABLE `la_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `module_fields`
--
ALTER TABLE `module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `module_field_types`
--
ALTER TABLE `module_field_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `role_module`
--
ALTER TABLE `role_module`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`);

--
-- Constraints for table `module_fields`
--
ALTER TABLE `module_fields`
  ADD CONSTRAINT `module_fields_field_type_foreign` FOREIGN KEY (`field_type`) REFERENCES `module_field_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `module_fields_module_foreign` FOREIGN KEY (`module`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_dept_foreign` FOREIGN KEY (`dept`) REFERENCES `departments` (`id`),
  ADD CONSTRAINT `roles_parent_foreign` FOREIGN KEY (`parent`) REFERENCES `roles` (`id`);

--
-- Constraints for table `role_module`
--
ALTER TABLE `role_module`
  ADD CONSTRAINT `role_module_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_module_fields`
--
ALTER TABLE `role_module_fields`
  ADD CONSTRAINT `role_module_fields_field_id_foreign` FOREIGN KEY (`field_id`) REFERENCES `module_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_module_fields_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `uploads`
--
ALTER TABLE `uploads`
  ADD CONSTRAINT `uploads_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

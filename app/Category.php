<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    //
    protected $table = 'categories';
    protected $fillable = ['category_name','category_menu_status'];
}

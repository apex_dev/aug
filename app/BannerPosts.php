<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerPosts extends Model
{
    //
    protected $table = "banner_posts";
    protected $fillable = ['banner_post_header','banner_post_body','banner_post_images_1','banner_post_images_2','banner_post_images_3','banner_post_images_4','banner_post_images_5','banner_post_status'];

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SubCategoryRequest;
use App\Sub_Categories;
use App\Categories;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sub = Sub_Categories::with('category')->get();
        return view('la.subcategories.index',compact('sub'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $cat = Categories::get();
        return view('la.subcategories.create',compact('cat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubCategoryRequest $request)
    {
        //
        $sub = New Sub_Categories($request->except('_token'));
        if($sub->save()){
            return redirect('admin/sub-categories')->with('success',trans('subcategory/message.success.create'));
        }else{
            return redirect('admin/sub-categories')->with('errors',trans('subcategory/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sub_Categories $sub)
    {
        //
        $cat = Categories::get();
        return view('la.subcategories.edit',compact('sub','cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubCategoryRequest $request, Sub_Categories $sub)
    {
        //
        if($sub->update($request->except('_token'))){
            return redirect('admin/sub-categories')->with('success',trans('subcategory/message.success.update'));
        }else{
            return redirect('admin/sub-categories')->with('errors',trans('subcategory/message.error.update'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sub_Categories $sub)
    {
        //
        if($sub->delete()){
            return redirect('admin/sub-categories')->with('success',trans('subcategory/message.success.delete'));
        }else{
            return redirect('admin/sub-categories')->with('errors',trans('subcategory/message.error.delete'));
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CategoryRequest;
use App\Categories;
use App\Sub_Categories;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $category = Categories::get();
        return view('la.categories.index',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('la.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        //
        $category = New Categories($request->except('_token'));
        if($category->save()){
            return redirect('admin/categories')->with('success',trans('category/message.success.create'));
        }else{
            return redirect('admin/categories')->with('errors',trans('category/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Categories $category)
    {
        //
        return view('la.categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request,Categories $category)
    {
        //
        if($category->update($request->except('_token'))){
            return redirect('admin/categories')->with('success',trans('category/message.success.update'));
        }else{
            return redirect('admin/categories')->with('errors',trans('category/message.error.update'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categories $category)
    {
        //
        $check = Sub_Categories::where('main_category_id',$category->id)->first();
        if(isset($check)){
            return redirect('admin/categories');
        }else{
            if($category->delete()){
                return redirect('admin/categories')->with('success',trans('category/message.success.delete'));
            }else{
                return redirect('admin/categories')->with('errors',trans('category/message.error.delete'));
            }
        }
    }
}

<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Upload;
use App\Posts;
use App\BannerPosts;
use App\Sub_Categories;
use App\Categories;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $banner_post = BannerPosts::orderby('id','DESC')->first();
        $category = Categories::with('sub_category')->orderby('category_name','ASC')->get();
        $general_post = Posts::join('sub_categories','posts.sub_category_id','=','sub_categories.id')->where('sub_categories.sub_category_tag','general')->where('posts.feature_post_status',1)->select('posts.*')->take(8)->get();
		return view('home',compact('banner_post','general_post','category'));
    }

    // ======================== Detail Page ============================== //

    public function post_detail(Posts $post){

        $category = Categories::with('sub_category')->orderby('category_name','ASC')->get();
        return view('detail',compact('post','category'));
    }

    public function categorized_posts($tag){

        $category = Categories::with('sub_category')->orderby('category_name','ASC')->get();
        $tag_post = Posts::join('sub_categories','posts.sub_category_id','=','sub_categories.id')->join('main_categories','sub_categories.main_category_id','=','main_categories.id')->where('main_categories.main_category_tag',$tag)->where('posts.feature_post_status',1)->select('posts.*')->orderBy('id','DESC')->get();
        return view('categorized_post',compact('tag_post','category'));
    }

    public function sub_categorized_posts($tag,$sub){

        $category = Categories::with('sub_category')->orderby('category_name','ASC')->get();
        $tag_post = Posts::join('sub_categories','posts.sub_category_id','=','sub_categories.id')->where('sub_categories.sub_category_tag',$sub)->where('posts.feature_post_status',1)->select('posts.*')->get();

        return view('categorized_post',compact('tag_post','category'));
    }

    public function about(){
        $category = Categories::with('sub_category')->orderby('category_name','ASC')->get();
        return view('about',compact('category'));
    }

    public function service(){
        $category = Categories::with('sub_category')->orderby('category_name','ASC')->get();
        return view('service',compact('category'));
    }

    public function company(){
        $category = Categories::with('sub_category')->orderby('category_name','ASC')->get();
        return view('company',compact('category'));
    }

    public function contact(){
        $category = Categories::with('sub_category')->orderby('category_name','ASC')->get();
        return view('contact',compact('category'));
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "post_header" => "required|max:50",
            'post_body_detail' => 'required',
            'sub_category_id' => 'required',
            'img_file_one' => 'image|mimes:jpg,jpeg,bmp,png',
            'img_file_two' => 'image|mimes:jpg,jpeg,bmp,png',
        ];
    }
}

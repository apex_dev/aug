<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannerPostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'banner_post_header' => 'required',
            'banner_post_body' => 'required',
        ];
    }
}

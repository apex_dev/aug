<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SubCategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'sub_category_name' => 'required',
            'sub_category_tag' => 'required',
            'main_category_id' => 'required',
        ];
    }
}

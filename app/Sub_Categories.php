<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sub_Categories extends Model
{
    //
    protected $table = 'sub_categories';
    protected $fillable = ['sub_category_name','main_category_id','sub_category_menu_status','sub_category_tag'];
    
    function post(){
    	return $this->hasMany('App\Posts','sub_category_id');
    }

    function category(){
    	return $this->belongsTo('App\Categories','main_category_id');
    }
}

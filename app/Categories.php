<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    //
    protected $table = "main_categories";
    protected $fillable = ['category_name','category_menu_status','main_category_tag'];

    public function sub_category(){
    	return $this->hasMany('App\Sub_Categories','main_category_id');
    }
    

}

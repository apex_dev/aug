<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    //
    protected $table = "posts";
    protected $fillable = ['post_header','post_body_detail','feature_post_status','post_image','feature_post_image','sub_category_id'];

    function category(){
    	return $this->belongsTo('App\Sub_Categories','sub_category_id');
    }
}

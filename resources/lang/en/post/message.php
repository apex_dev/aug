<?php 

return [

	'success' => array(
        'create'    => 'Post was successfully created.',
        'update'    => 'Post was successfully updated.',
        'delete'    => 'Post was successfully deleted.',
    ),

    'errors' => array(
        'create'    => 'There was an issue creating the Post. Please try again.',
        'update'    => 'There was an issue updating the Post. Please try again.',
        'delete'    => 'There was an issue deleting the Post. Please try again.',
    ),

]

 ?>
<?php 

return [

	'success' => array(
        'create'    => 'Category was successfully created.',
        'update'    => 'Category was successfully updated.',
        'delete'    => 'Category was successfully deleted.',
    ),

    'errors' => array(
        'create'    => 'There was an issue creating the category. Please try again.',
        'update'    => 'There was an issue updating the category. Please try again.',
        'delete'    => 'There was an issue deleting the category. Please try again.',
    ),

]

 ?>
<?php 

return [

	'success' => array(
        'create'    => 'Banner Post was successfully created.',
        'update'    => 'Banner Post was successfully updated.',
        'delete'    => 'Banner Post was successfully deleted.',
    ),

    'errors' => array(
        'create'    => 'There was an issue creating the Banner Post. Please try again.',
        'update'    => 'There was an issue updating the Banner Post. Please try again.',
        'delete'    => 'There was an issue deleting the Banner Post. Please try again.',
    ),

]

 ?>
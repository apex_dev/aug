@extends("la.layouts.app")

@section("contentheader_title", "Posts")
@section("contentheader_description", "Posts listing")
@section("section", "Posts")
@section("sub_section", "Listing")
@section("htmlheader_title", "Posts Listing")

@section("headerElems")
  <a href="{{url('admin/posts/create')}}"><button class="btn btn-success btn-sm pull-right">Add Post</button></a>
@endsection

@section('main-content')
    @if ($message = Session::get('success'))
    <div class="alert alert-success" id="popup">
      <a href="javascript:closeMsg();"><i class="fa fa-close" style="float: right;cursor: pointer;"></i></a>
        <p>            
            {{$message}}
        </p>
    </div>
    @endif
  
  <div class="box box-success">
    <!--<div class="box-header"></div>-->
    <div class="box-body">
      <table id="example1" class="table table-bordered">
        <thead>
        <tr class="success">
          <th>Post Header</th>
          <th>Post Body</th>
          <th>Feature Post Status</th>
          <th>Post Category</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($post))
        @foreach($post as $posts)
          <tr>
            <td>{{$posts->post_header}}</td>
            <td>{{str_limit($posts->post_body_detail,50)}}</td>
            @if($posts->feature_post_status == 1)
            <td>Yes</td>
            @else
            <td>No</td>
            @endif
            @if(!empty($posts->category))
            <td>{{$posts->category->sub_category_name}}</td>
            @else
            <td>Not Categorized Yet</td>
            @endif

            <td>
              <a class="btn btn-warning btn-xs" href="{{url('/admin/posts/edit/'.$posts->id)}}" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" href="{{url('/admin/posts/delete/'.$posts->id)}}" style="display:inline;padding:2px 5px 3px 5px;" onclick="return confirm('Are you sure to delete this category?')"><i class="fa fa-remove"></i></a>
            </td>
          </tr>
        @endforeach
        @endif
        </tbody>
      </table>
    </div>
  </div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script type="text/javascript">
  function closeMsg()
  {
    $("#popup").fadeOut();
  }
</script>
<!--  -->
@endpush
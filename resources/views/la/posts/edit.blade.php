@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/posts') }}">Post</a> :
@endsection
@section("contentheader_description", "Create Form")
@section("section", "Posts")
@section("section_url", url(config('laraadmin.adminRoute') . '/posts'))
@section("sub_section", "Edit")



@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- <div class="col-md-12">
<h6>UPloaded Successfully.</h6>
</div> -->
<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form action="{{url('admin/posts/edit/'.$post->id)}}" method="POST" enctype="multipart/form-data" files=true>
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<div class="form-group">
						<label for="post_header">Post Header* :</label>
						<input class="form-control" placeholder="Enter Post Header" data-rule-minlength="1" data-rule-maxlength="200" required="1" name="post_header" aria-required="true" type="text" value="{{$post->post_header}}">
					</div>
					<div class="form-group">
						<label for="post_body">Post Body* :</label>
						<textarea class="form-control" placeholder="Enter Post Detail" name="post_body_detail" id="texteditor">{{$post->post_body_detail}}</textarea>
					</div>
					<div class="form-group">
						<label for="sub_category_id">Post Category :</label>
						<select class="form-control select2-hidden-accessible" data-placeholder="Enter Post Category" rel="select2" name="sub_category_id" tabindex="-1" aria-hidden="true">
						@if(isset($sub))
						@foreach($sub as $subs)
							<option value="{{$subs->id}}" @if($post->sub_category_id == $subs->id) selected @endif>{{$subs->sub_category_name}}</option>
						@endforeach
						@endif
						</select>
					</div>
					<div class="form-group">
						<label for="feature_post_status">Appear As Feacture Post? : </label>
						<br>
						<div class="radio">
							<label>
							<input name="feature_post_status" value=1 type="radio" @if($post->feature_post_status == 1) checked @endif>
							Yes
							</label>
							<label>
							<input name="feature_post_status" value=0 type="radio" @if($post->feature_post_status == 0) checked @endif>
							No
							</label>
						</div>
					</div>
<br>
					


					<div class="form-group col-md-6">
							<label for="post_image">Image For Detail Post: </label>
						<div class="fileinput fileinput-new" data-provides="fileinput">
						  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
						@if(!empty($post->post_image))
							<img src="{{asset('uploads/posts/'.$post->post_image)}}" alt="Upload Image">
						@else
						    <img src="holder.js/100%x100%" alt="Upload Image">
						@endif
						  </div>
						  <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"></div>
						  <div>
						    <span class="btn btn-primary btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="img_file_one"></span>
						    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
						  </div>
						  <h5 style="color: red;"> *Width x Height must be around 726 x 422 pixels*</h5>
						</div>
					</div>

					<div class="form-group col-md-6"><br><br>
							<label for="post_image">Image For Feature Post: </label>
						<div class="fileinput fileinput-new" data-provides="fileinput">
						  <div class="fileinput-new thumbnail" style="width: 130px; height: 100px;">
						@if(!empty($post->feature_post_image))
							<img src="{{asset('uploads/posts/'.$post->feature_post_image)}}" alt="Upload Image">
						@else
						    <img src="holder.js/100%x100%" alt="Upload Image">
						@endif
						  </div>
						  <div class="fileinput-preview fileinput-exists thumbnail" style="width: 130px; height: 100px;"></div>
						  <div>
						    <span class="btn btn-primary btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="img_file_two"></span>
						    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
						  </div>
						  <h5 style="color: red;"> *Width x Height must be around 252 x 147 pixels*</h5>
						</div>
					</div>
				
					<div class="form-group pull-right">					
						<input class="btn btn-success " value="Submit" type="submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script src="{{asset('assets/js/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('assets/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
<script>
	tinymce.init({
    selector: '#texteditor',
  });
</script>
@endpush

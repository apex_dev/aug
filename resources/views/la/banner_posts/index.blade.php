@extends("la.layouts.app")

@section("contentheader_title", "Banner Posts")
@section("contentheader_description", "Banner Posts listing")
@section("section", "Banner Posts")
@section("sub_section", "Listing")
@section("htmlheader_title", "Banner Posts Listing")

@section("headerElems")
  <a href="{{url('admin/banner_posts/create')}}"><button class="btn btn-success btn-sm pull-right">Add Banner Post</button></a>
@endsection

@section('main-content')

  @if ($message = Session::get('success'))
    <div class="alert alert-success" id="popup">
      <a href="javascript:closeMsg();"><i class="fa fa-close" style="float: right;cursor: pointer;"></i></a>
        <p>            
            {{$message}}
        </p>
    </div>
  @endif

  <div class="box box-success">
    <!--<div class="box-header"></div>-->
    <div class="box-body">
      <table id="example1" class="table table-bordered">
        <thead>
        <tr class="success">
          <th>Post Header</th>
          <th>Post Body</th>
          <th>Appear On Front Page</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @if(isset($banners))
        @foreach($banners as $banner)
          <tr>
            <td>{{$banner->banner_post_header}}</td>
            <td>{{str_limit($banner->banner_post_body,50)}}</td>
            @if($banner->banner_post_status == 1)
            <td>Yes</td>
            @else
            <td>No</td>
            @endif
            <td>
              <a class="btn btn-warning btn-xs" href="{{url('/admin/banner_posts/edit/'.$banner->id)}}" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" href="{{url('/admin/banner_posts/delete/'.$banner->id)}}" style="display:inline;padding:2px 5px 3px 5px;" onclick="return confirm('Are you sure to delete this category?')"><i class="fa fa-remove"></i></a>
            </td>
          </tr>
        @endforeach
        @endif
        </tbody>
      </table>
    </div>
  </div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script type="text/javascript">
  function closeMsg()
  {
    $("#popup").fadeOut();
  }
</script>
<!--  -->
@endpush
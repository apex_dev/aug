@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/banner_posts') }}">Banner Post</a> :
@endsection
@section("contentheader_description", "Create Form")
@section("section", "Banner Posts")
@section("section_url", url(config('laraadmin.adminRoute') . '/banner_posts'))
@section("sub_section", "Edit")



@section("main-content")
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form action="{{url('admin/banner_posts/edit/'.$banner->id)}}" method="POST" enctype="multipart/form-data" files=true>
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<div class="form-group">
						<label for="post_header">Post Header* :</label>
						<input class="form-control" placeholder="Enter Post Header" data-rule-minlength="1" data-rule-maxlength="200" name="banner_post_header" aria-required="true" type="text" value="{{$banner->banner_post_header}}">
					</div>
					<div class="form-group">
						<label for="post_body">Post Body* :</label>
						<textarea class="form-control" placeholder="Enter Post Detail" name="banner_post_body">{{$banner->banner_post_body}}</textarea>
					</div>
					<div class="form-group">
						<label for="banner_post_status">Appear As Feacture Post? : </label>
						<br>
						<div class="radio">
							<label>
							<input name="banner_post_status" value=1 type="radio" @if($banner->banner_post_status == 1) checked @endif>
							Yes
							</label>
							<label>
							<input name="banner_post_status" value=0 type="radio" @if($banner->banner_post_status == 0) checked @endif>
							No
							</label>
						</div>
					</div>
					<div class="form-group">
							<label class="col-md-12" for="post_image">Images For Slider : </label>
						<div class="fileinput fileinput-new" data-provides="fileinput">
						  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
						    <img src="{{asset('uploads/bannerposts/'.$banner->banner_post_images_1)}}" alt="Upload Image">
						  </div>
						  <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"></div>
						  <div>
						    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="img_file_1"></span>
						    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
						  </div>
						</div>
						<div class="fileinput fileinput-new" data-provides="fileinput">
						  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
						    <img src="{{asset('uploads/bannerposts/'.$banner->banner_post_images_2)}}" alt="Upload Image">
						  </div>
						  <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"></div>
						  <div>
						    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="img_file_2"></span>
						    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
						  </div>
						 </div>
						 <div class="fileinput fileinput-new" data-provides="fileinput">
						  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
						    <img src="{{asset('uploads/bannerposts/'.$banner->banner_post_images_3)}}" alt="Upload Image">
						  </div>
						  <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"></div>
						  <div>
						    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="img_file_3"></span>
						    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
						  </div>
						 </div>
						 <div class="fileinput fileinput-new" data-provides="fileinput">
						  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
						    <img src="{{asset('uploads/bannerposts/'.$banner->banner_post_images_4)}}" alt="Upload Image">
						  </div>
						  <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"></div>
						  <div>
						    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="img_file_4"></span>
						    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
						  </div>
						 </div>
						 <div class="fileinput fileinput-new" data-provides="fileinput">
						  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
						    <img src="{{asset('uploads/bannerposts/'.$banner->banner_post_images_5)}}" alt="Upload Image">
						  </div>
						  <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"></div>
						  <div>
						    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="img_file_5"></span>
						    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
						  </div>
						 </div>

					</div>
					<div class="form-group">
						<input class="btn btn-success " value="Submit" type="submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/tinymce/tinymce.min.js')}}"></script>
<script src="{{ asset('assets/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
<script>
	tinymce.init({
    selector: '#texteditor',
  });
</script>
<script>
	$(document).ready(function(){
	 var imgWidth = $('#target_img').width();
	 var imgHeight = $('#target_img').height();
	 if(imgWidth > 400 || imgHeight > 200){
	 alert('Your image is too big, it must be less than 200x400');
}
});
</script>
@endpush

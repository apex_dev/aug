<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        @if(LAConfigs::getByKey('sidebar_search'))
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
	                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        @endif
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{ url(config('laraadmin.adminRoute')) }}"><i class='fa fa-home'></i> <span>Dashboard</span></a></li>
            <!-- <?php
            $menuItems = Dwij\Laraadmin\Models\Menu::where("parent", 0)->orderBy('hierarchy', 'asc')->get();
            ?> -->
            <!-- @foreach ($menuItems as $menu)
                @if($menu->type == "module")
                    <?php
                    $temp_module_obj = Module::get($menu->name);
                    ?>
                    @la_access($temp_module_obj->id)
						@if(isset($module->id) && $module->name == $menu->name)
                        	<?php echo LAHelper::print_menu($menu ,true); ?>
						@else
							<?php echo LAHelper::print_menu($menu); ?>
						@endif
                    @endla_access
                @else
                    <?php echo LAHelper::print_menu($menu); ?>
                @endif
            @endforeach -->
            <!-- LAMenus -->
            <li @if( Request::is('admin/users') || Request::is('admin/users/*')) class="active" @endif ><a href="{{ url('admin/users') }}"><i class='fa fa-user'></i> <span>Users</span></a></li>
            <li @if( Request::is('admin/posts') || Request::is('admin/posts/*')) class="active" @endif ><a href="{{ url('admin/posts') }}"><i class='fa fa-paper-plane'></i> <span>Posts</span></a></li>
            <li @if( Request::is('admin/banner_posts') || Request::is('admin/banner_posts/*')) class="active" @endif ><a href="{{ url('admin/banner_posts') }}"><i class='fa fa-paper-plane'></i> <span>Banner Post</span></a></li>
            <li @if( Request::is('admin/categories') || Request::is('admin/categories/*')) class="active" @endif ><a href="{{ url('admin/categories') }}"><i class='fa fa-list'></i> <span>Main Category</span></a></li>
            <li @if( Request::is('admin/sub-categories') || Request::is('admin/sub-categories/*')) class="active" @endif ><a href="{{ url('admin/sub-categories') }}"><i class='fa fa-list-alt'></i> <span>Sub-Category</span></a></li>
            
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

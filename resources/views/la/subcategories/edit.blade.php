@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/posts') }}">Post</a> :
@endsection
@section("contentheader_description", "Create Form")
@section("section", "Posts")
@section("section_url", url(config('laraadmin.adminRoute') . '/posts'))
@section("sub_section", "Edit")



@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form action="{{url('admin/sub-categories/edit/'.$sub->id)}}" method="POST">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<div class="form-group">
						<label for="sub_category_name">Sub-Category Name* :</label>
						<input class="form-control" placeholder="Sub-Category Name" data-rule-minlength="1" data-rule-maxlength="200" required="1" name="sub_category_name" aria-required="true" type="text" value="{{$sub->sub_category_name}}">
					</div>
					<div class="form-group">
						<label for="sub_category_tag">Keyword/Tag* :</label>
						<input class="form-control" placeholder="Enter Category Name" data-rule-minlength="1" data-rule-maxlength="200" required="1" name="sub_category_tag" aria-required="true" value="{{$sub->sub_category_tag}}" type="text">
					</div>
					<div class="form-group">
						<label for="main_category_id">Main Category :</label>
						<select class="form-control select2-hidden-accessible" data-placeholder="Enter Post Category" rel="select2" name="main_category_id" tabindex="-1" aria-hidden="true">
						@if(isset($cat))
						@foreach($cat as $cats)
							<option value="{{$cats->id}}" @if($cats->id == $sub->main_category_id) selected @endif>{{$cats->category_name}}</option>
						@endforeach
						@endif
						</select>
					</div>
<!-- 					<div class="form-group">
						<label for="feature_post_status">Appear As Feacture Post? : </label>
						<br>
						<div class="radio">
							<label>
							<input name="feature_post_status" value=1 type="radio">
							Yes
							</label>
							<label>
							<input name="feature_post_status" value=0 type="radio">
							No
							</label>
						</div>
					</div> -->
					<div class="form-group">
						<input class="btn btn-success pull-right" value="Submit" type="submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#user-edit-form").validate({
		
	});
});
</script>
@endpush

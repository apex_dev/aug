@extends("la.layouts.app")

@section("contentheader_title", "Sub-Categories")
@section("contentheader_description", "Sub-Categories Listing")
@section("section", "Sub-Categories")
@section("sub_section", "Listing")
@section("htmlheader_title", "Sub-Categories Listing")

@section("headerElems")
  <a href="{{url('admin/sub-categories/create')}}"><button class="btn btn-success btn-sm pull-right">Add Sub-Category</button></a>
@endsection

@section('main-content')

  @if ($message = Session::get('success'))
    <div class="alert alert-success" id="popup">
      <a href="javascript:closeMsg();"><i class="fa fa-close" style="float: right;cursor: pointer;"></i></a>
        <p>            
            {{$message}}
        </p>
    </div>
  @endif

  <div class="box box-success">
    <!--<div class="box-header"></div>-->
    <div class="box-body">
      <table id="example1" class="table table-bordered">
        <thead>
        <tr class="success">
          <th>Sub-Category Name</th>
          <th>Keyword / Tag</th>
          <th>Parent Category Name</th>
          <th>Show on Menu</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
          @foreach($sub as $subs)
          <tr>
            <td>{{$subs->sub_category_name}}</td>
            <td>{{$subs->sub_category_tag}}</td>
            <td>{{$subs->category->category_name}}</td>
            @if($subs->sub_category_menu_status == 1)
            <td>Yes</td>
            @else
            <td>No</td>
            @endif
            <td>
              <a class="btn btn-warning btn-xs" href="{{url('/admin/sub-categories/edit/'.$subs->id)}}" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" href="{{url('/admin/sub-categories/delete/'.$subs->id)}}" style="display:inline;padding:2px 5px 3px 5px;" onclick="return confirm('Are you sure to delete this category?')"><i class="fa fa-remove"></i></a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script type="text/javascript">
  function closeMsg()
  {
    $("#popup").fadeOut();
  }
</script>
<!--  -->
@endpush
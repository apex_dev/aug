@extends("la.layouts.app")

@section("contentheader_title", "Category")
@section("contentheader_description", "Category Listing")
@section("section", "Category")
@section("sub_section", "Listing")
@section("htmlheader_title", "Category Listing")

@section("headerElems")
  <a href="{{url('admin/categories/create')}}"><button class="btn btn-success btn-sm pull-right">Add Category</button></a>
@endsection

@section('main-content')

  @if ($message = Session::get('success'))
    <div class="alert alert-success" id="popup">
      <a href="javascript:closeMsg();"><i class="fa fa-close" style="float: right;cursor: pointer;"></i></a>
        <p>            
            {{$message}}
        </p>
    </div>
  @endif

  <div class="box box-success">
    <!--<div class="box-header"></div>-->
    <div class="box-body">
      <table id="example1" class="table table-bordered">
        <thead>
        <tr class="success">
          <th>Category Name</th>
          <th>Keyword / Tag</th>
          <th>Show on Menu</th>
          <th>Actions</th>
        </tr>
        </thead>
        <tbody>
          @if(isset($category))
          @foreach($category as $cats)
          <tr>
            <td>{{$cats->category_name}}</td>
            <td>{{$cats->main_category_tag}}</td>
            @if($cats->category_menu_status == 1)
            <td>Yes</td>
            @else
            <td>No</td>
            @endif
            <td>
              <a class="btn btn-warning btn-xs" href="{{url('/admin/categories/edit/'.$cats->id)}}" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
              <a class="btn btn-danger btn-xs" href="{{url('/admin/categories/delete/'.$cats->id)}}"  onclick="return confirm('Are you sure to delete this category?')" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-remove"></i></a>
            </td>
          </tr>
          @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>
@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script type="text/javascript">
  function closeMsg()
  {
    $("#popup").fadeOut();
  }
</script>
<!--  -->
@endpush
@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/categories') }}">Category</a> :
@endsection
@section("contentheader_description", "Create Form")
@section("section", "Categories")
@section("section_url", url(config('laraadmin.adminRoute') . '/categories'))
@section("sub_section", "Create")



@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form action="{{url('admin/categories/create')}}" method="POST">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<div class="form-group">
						<label for="post_header">Category Name* :</label>
						<input class="form-control" placeholder="Enter Category Name" name="category_name" type="text">
					</div>
					<div class="form-group">
						<label for="main_category_tag">Keyword/Tag* :</label>
						<input class="form-control" placeholder="Enter Category Tag" name="main_category_tag" type="text">
					</div>
					<input name="category_menu_status" value=1 type="hidden">
					<!-- <div class="form-group">
						<label for="category_menu_status">Appear As Feacture Post? : </label>
						<br>
						<div class="radio">
							<label>
							<input name="category_menu_status" value=1 type="radio">
							Yes
							</label>
							<label>
							<input name="category_menu_status" value=0 type="radio">
							No
							</label>
						</div>
					</div> -->
					<div class="form-group">
						<input class="btn btn-success pull-right" value="Submit" type="submit">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#user-edit-form").validate({
		
	});
});
</script>
@endpush

@extends('master')
@section('style')  

    <title>Detail page</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/component.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom-styles.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" />     
	  <link rel="stylesheet" href="{{asset('assets/css/demo.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
@endsection
@section('content')

    <div class="container" >
      <div class="featured-block">
        <!-- Example row of columns -->
        <div class="row">
        @foreach($tag_post as $posts)
          <div class="col-md-3">
            <div class="block">
            <div class="thumbnail" style="height:370px;">
              <img src="{{asset('uploads/posts/'.$posts->post_image)}}" style="width:254px;height:158px;" alt="" class="img-responsive">
              <div class="caption">
               <div style="height:134px;">
                  <h1>{!! $posts->post_header !!}</h1>
                  <?php $post_body = strip_tags($posts->post_body_detail); ?>
                  <p>{!! str_limit($post_body,50) !!}</p>
                </div>
                <a class="btn" href="{{url('post/'.$posts->id.'/detail')}}">more</a>
              </div>
              </div>
            </div>
            </div>
          @endforeach
            
          </div>
         </div>
        </div>

@endsection
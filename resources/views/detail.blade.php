@extends('master')
@section('style')  

    <title>Detail Page</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/component.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom-styles.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" />     
	  <link rel="stylesheet" href="{{asset('assets/css/demo.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/portfolio.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.theme.css') }}">
 
@endsection
@section('content')


    <div class="featured-block" style="background-color: #fff;">
      <div class="container" >
        <!-- Example row of columns -->
        <div class="row">
          <div class="col-md-12">
            <h2 style="font-weight: bold;">{{$post->post_header}}</h2>
          </div>
        </div><br>
        <div class="row">
          <div class="col-md-6 wow bounceInLeft" data-wow-duration="1.5s">
            <div class="block">          
              <img src="{{asset('uploads/posts/'.$post->post_image)}}" alt="" class="img-responsive">
            </div>
          </div>

            <div class="col-md-6 wow bounceInRight" data-wow-duration="1.5s"  style="padding:0px 20px;text-align: justify;">
                {!!$post->post_body_detail!!}
            </div>
        </div>

      </div>
    </div>

@endsection
@section('scripts')
  
    <script src="{{asset('assets/js/jquery-1.9.1.js')}}"></script>  
    <script src="{{asset('assets/js/carousel.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/wow.min.js')}}"></script>

@endsection

<html lang="en">
  <meta charset="utf-8">
  <head> 


    @yield('style')
 
  </head>

  <body>
   <div id="header" style="background: #fff;">
    <div>
      <div class=" col-md-1" style="margin-left: 5em;">
        
          <a href="{{url('/')}}"><img src="{{asset('assets/image/AUG.png')}}" alt="" style="width:150px ; height:60px;"></a>
         
      </div>

        <div class= "col-md-1 col-md-offset-5" >
            <ul class="nav navbar-nav">
              <li><a href="{{url('/')}}"><i class="fa fa-home">Home</i></a></li>
           <!--    <li><a href="{{url('about')}}">About Us</a></li>          
      
              <li><a href="{{url('service')}}">Service</a></li> -->
              
            </ul>

        </div>
        <div class="col-md-4 col-md-offset-" id="socialicons" style="margin-top: 1.2em;">
              
               <!--  <a class="facebook" id="icon" href="https://www.facebook.com/daweidevelopmentpublic/" title="Follow with Facebook" target="_blank"><i class="fa fa-facebook"></i></a>  &nbsp;&nbsp; -->
               <i class="fa fa-phone" style="color:#9E131B;font-size: 20px;"></i>&nbsp;<label><span style="font-weight: normal;">
                         95-9-5008646 </span></label>&nbsp;&nbsp;
                <i class="fa fa-envelope" style="color:#9E131B;font-size: 20px;"></i>&nbsp;<label><span class="text-" style="font-weight: normal;">
                        apexuniongas@gmail.com</span></label>
        </div>

    </div>

  </div>

 <div class="menu">
      <div class="navbar">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <i class="fw-icon-th-list"></i>
            </button>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav-var">
              @foreach($category as $categories)
              <li onClick="toggleSub()">
                <a href="{{url('post/'.$categories->main_category_tag)}}">{{$categories->category_name}}</a>              
                  <ul class ="sub" id="sub">
                  @foreach($categories->sub_category as $sub_categories)
                    <li><a href = "{{url('post/'.$categories->main_category_tag.'/'.$sub_categories->sub_category_tag)}}">
                      {{$sub_categories->sub_category_name}}
                    </a></li>
                  @endforeach
                  </ul>
              </li>
              @endforeach
              
           
            </ul>

          </div><!--/.navbar-collapse -->

        </div>
      </div>
      <!--   <div class="mini-menu">
            <label>
            <select class="selectnav">
        
            <option value="" selected=""><a href="">Education</a></option>
        
            <option value="#"><a href="">Shopping Center</a></option>
            <option value="#"><a href="">Wedding Hall</a></option>
            <option value="#"><a href="">Petrol Station</a></option>
            <option value="#"><a href="">Power Distribution</a></option>
            <option value="#"><a href="">Project</a></option>
            <option value="#"><a href="">Maungmagun Hotel Zone</a></option>

    
          </select>
          </label>
      </div> -->
    </div>

    @yield('content')
    <div class="footer-wrapper">        
      <div class="copy-rights">
        <div class="container">                
          <div class="col-sm-4">
           <h4 style="margin-bottom: 1em;margin-top: 1em;margin-left:30px;">Recent Post</h4>
                <ul>
               
                </ul>
            </div>
            <div class="col-sm-4 col-sm-offset-" >
                <h4 style="margin-bottom: 1em;margin-top: 1em;">Let's Keep In Touch!</h4>
                <ul class="list-unstyled">
                    <li>PlotNo.1 & 2(B), Thilawar,Kyauk Tan Township, <br>Yangon, Myanmar</li>
                    <li>Phone: 09-5008646 </li>                    
                    <li>Email : apexuniongas@gmail.com <span class="text-success" style="cursor: pointer;">
                    </span>
                   
                  
                </ul>
               
            </div>

             <div class="col-sm-3 col-sm-offset-1 ">
                 <h4 style="margin-top: 1em;margin-left: 2.5em;">Follow Us On<span style="background: #22407F;font-size: 15px;padding:1px 5px 1px 5px;border-radius: 5px;">facebook</span></h4><br>
             
                <!-- <div id="socialicons">
                  <a class="twitter" id="icon" href="LINK" title="Follow with Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                  <a class="facebook" id="icon" href="LINK" title="Follow with Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                  <a class="instagram" id="icon" href="LINK" title="Follow with Instagram" target="_blank"><i class="fa fa-instagram"></i></a>
                  <a class="google-plus" id="icon" href="LINK" title="Follow with Google Plus" target="_blank"><i class="fa fa-google-plus"></i></a>

                  <a class="pinterest" id="icon" href="LINK" title="Follow with Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a>
                  <a class="youtube" id="icon" href="LINK" title="Follow with YouTube" target="_blank"><i class="fa fa-youtube"></i></a>

                </div> --> 
                <div id="socialicons">
                  <ul style="list-style: none;" class="pull-left">
                   
                  <!--   <li ><i class="fa fa-facebook facebook" id="icon"></i><a  href="https://www.facebook.com/ddpcshoppingcenter/" title="DDPC Shopping Center" target="_blank">DDPC Shopping Center</a></li>
                    <li><i class="fa fa-facebook facebook" id="icon"></i><a  href="https://www.facebook.com/ddpcsupermarket/" title="DDPC Supermarket" target="_blank">Power Distribution</a></li>
                    <li><i class="fa fa-facebook facebook" id="icon"></i><a  href="https://www.facebook.com/ddpcroyalpalace/" title="Royal Palace Hall" target="_blank">Royal Palace Hall</a></li>
                    <li><i class="fa fa-facebook facebook" id="icon"></i><a  href="https://www.facebook.com/edupark.dawei/" title="EduPark-Dawei" target="_blank">EduPark-Dawei</a></li>
                    <li><i class="fa fa-facebook facebook" id="icon"></i><a  href="https://www.facebook.com/ddpcroyalcoffee/" title="Royal Coffee Shop" target="_blank">Royal Coffee Shop</a></li>
                    <li><i class="fa fa-facebook facebook" id="icon"></i><a  href="https://www.facebook.com/DDPCdfashion/" title="dfashion" target="_blank">dfashion</a></li> -->

                  </ul>                 
                </div>           
            </div>           
        </div>
      </div>
  </div>
      <!--  <div class="last_footer" >
       
          <div class="col-md-11 text-center" style="background: #3B3E4A;" >
              <p>Copyright © 2017- All Rights Reserved.</p>
          </div>
          <div class="col-md-1" style="background: #3B3E4A;">
             <a id="scrollUp" href="" style=" z-index: 2147483647; display: block;"></a>
          </div>
        </div> -->
        </div>
</body>
      @yield('scripts')
</html>
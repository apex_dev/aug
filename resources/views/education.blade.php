@extends('master')
@section('style')
<title>Education</title>
<link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/component.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom-styles.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" />     
    <link rel="stylesheet" href="{{asset('assets/css/demo.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
@endsection
@section('content')
    <div class="container" >
      <div class="featured-block">
        <!-- Example row of columns -->
        <div class="row">
          <div class="col-md-3">
            <div class="block">
            <div class="thumbnail">
              <img src="image/1.jpg" alt="" class="img-responsive">
              <div class="caption">
                <h1>Lorem ipsum</h1>
                <p>Cuibstui ipsum dolor sit amet, consectetuer adipiscing eli onec odio cuisque volutpat mattis</p>
                <a class="btn" href="moredetail.php">more</a>
              </div>
              </div>
            </div>
            </div>
            <div class="col-md-3">
              <div class="block">
                <div class="thumbnail">
                    <img src="image/2.jpg" alt="" class="img-responsive">
                    <div class="caption">
                      <h1>Fusce lacinia arcu</h1>
                      <p>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis ctor, ultrices ut, element</p>
                      <a class="btn" href="#">more</a>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="block">
                <div class="thumbnail">
                  <img src="image/1.jpg" alt="" class="img-responsive">
                  <div class="caption">
                    <h1>Morbiin semquis</h1>
                    <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean</p>
                    <a class="btn" href="#">more</a>
                  </div>
                  </div>
              </div>
            </div>
            <div class="col-md-3">
            <div class="block">
            <div class="thumbnail">
              <img src="image/2.jpg" alt="" class="img-responsive">
              <div class="caption">
                <h1>Aliquam lectus</h1>
                <p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, mod in, pharetra ultrici</p>
                <a class="btn" href="#">more</a>
              </div>
              </div>
            </div>
            </div>
            
          </div>
         </div>
        </div>

@endsection
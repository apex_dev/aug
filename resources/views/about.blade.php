@extends('master')
@section('style')
 <!--    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">   --> 

    <title>ABOUT | Apex Union Gas Company</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/component.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom-styles.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" />     
	<link rel="stylesheet" href="{{asset('assets/css/demo.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/portfolio.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.theme.css') }}">
    <style type="text/css">
        body{
            background: #fff;
        }
    </style>

@endsection
@section('content')

  <div class="container" >
   <div class="featured-block">
        <div class="row details">
            <h2 id="single_portfolio_title"><label> Welcome To AUG</label></h2><br>
            <!-- Slider Section Start -->
            <div class="col-md-6 wow bounceInLeft" data-wow-duration="1.5s">
                <div class="col-md-5 col-sm-12 slider">
                    <div id="owl-demo" class="owl-carousel owl-theme">
                        <div class="item"><img src="{{ asset('assets/image/banner/7.jpg') }}" alt="slider-image" class="img-responsive" style="width: 559px; height: 322px;">
                        </div>
                        <div class="item"><img src="{{ asset('assets/image/banner/4.jpg') }}" alt="slider-image" class="img-responsive" style="width: 559px; height: 322px;">
                        </div>
                        <div class="item"><img src="{{ asset('assets/image/banner/6.jpg') }}" alt="slider-image" class="img-responsive" style="width: 559px; height: 322px;">
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Slider Section End -->
            <!-- Project Description Section Start -->
            <div class="col-md-6 col-sm-12 col-xs-12 wow bounceInRight" data-wow-duration="1.5s">
                <h3 class="project">About Us</h3><br>
                <p><h5 style="text-align: justify;line-height: 20px;">
                    Coming Soon...          
                </p></h5>
               
            </div>
            <!-- //Project Description Section End -->
        </div>
    </div>
        <!-- Related Section Start -->
     
        <!-- Related Setion End -->
</div>
        
@endsection
@section('scripts')
  
    <script src="{{asset('assets/js/jquery-1.9.1.js')}}"></script>  
    <script src="{{asset('assets/js/carousel.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/wow.min.js')}}"></script>

@endsection
@extends('master')
@section('style')
 <!--    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">   --> 

    <title>CONTACT | Apex Union Gas Company</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/component.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom-styles.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" />     
      <link rel="stylesheet" href="{{asset('assets/css/demo.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
     <link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.min.css')}}">
     <style type="text/css">
         body{
            background: #fff;
         }
     </style>
 

@endsection
@section('content')

 <div class="container">
       <div class="featured-block  wow bounceInRight" data-wow-duration="1.5s">
        <div class="row">
            <!-- Contact form Section Start -->
            <div class="col-md-6 col-md-offset-1">
                <h3>Contact Form</h3><br>
                <!-- Notifications -->
            

                <form class="contact" id="contact" action="{{url('admin/subscribers/create')}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group">
                        <input type="text" name="subscriber_name" class="form-control " placeholder="Your name" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="subscriber_email" class="form-control" placeholder="Your email address" required>
                    </div>
                    <div class="form-group">
                        <textarea name="subscriber_comment" class="form-control no-resize resize_vertical" rows="6" placeholder="Your comment" required></textarea>
                    </div>
                    <div class="input-group">
                        <button class="btn btn-primary" type="submit">submit</button>
                        <button class="btn btn-danger" type="reset">cancel</button>
                    </div>
                </form>
            </div>
            <!-- //Conatc Form Section End -->
            <!-- Address Section Start -->
            <div class="col-md-4 col-sm-4" id="address_margt">
                <div class="media media-right">
                    <div class="media-left media-top">
                        <a href="#">
                            <div class="box-icon">
                                <i class="livicon" data-name="home" data-size="22" data-loop="true" data-c="#fff" data-hc="#fff"></i>
                            </div>
                        </a>
                    </div>
                    <div class="media-body">
                    <i class="fa fa-home" aria-hidden="true" style="font-size: 50px;color:#B2070E;" ></i>
                        <h4 class="media-heading">Address:</h4>
                  
                 
                    <ul class="list-unstyled">
                    <li>PlotNo.1 & 2(B), Thilawar,Kyauk Tan Township, <br>Yangon, Myanmar</li>
                   
                    </span>
                   
                  
                
                    </span>
                   
                  
                </ul>
                    </div>
                </div>
                <div class="media padleft10">
                    <div class="media-left media-top">
                        <a href="#">
                            <div class="box-icon">
                                <i class="livicon" data-name="phone" data-size="22" data-loop="true" data-c="#fff" data-hc="#fff"></i>
                            </div>
                        </a>
                    </div>
                    <div class="media-body padbtm2">
                    <i class="fa fa-phone-square " aria-hidden="true" style="font-size: 50px;color:#B2070E;" ></i>
                    <h4 class="media-heading">Telephone:</h4>
                    <ul class="list-unstyled">
                           <li>Phone: 09-5008646 </li>                    
                    <li>Email : apexuniongas@gmail.com </li>
                    </ul>
                    </div>
                </div>
            </div>
            <!-- //Address Section End -->
        </div>
    </div>
</div>
        
        
@endsection
@section('scripts')
  
    <script src="{{asset('assets/js/jquery-1.9.1.js')}}"></script>  
    <script src="{{asset('assets/js/carousel.js')}}"></script>
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
@endsection
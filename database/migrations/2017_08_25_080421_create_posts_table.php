<?php
/**
 * Migration genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // This Module has been deleted.
        // You can remove this file after migrate:reset
        
            Schema::create('posts',function(Blueprint $table){
                $table->increments('id');
                $table->string('sub_category_id');
                $table->string('post_header');
                $table->text('post_body_detail');
                $table->string('feature_post_status')->nullable();
                $table->string('post_image');
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::drop('posts');
    }
}

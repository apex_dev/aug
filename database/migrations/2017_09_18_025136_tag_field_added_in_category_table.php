<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TagFieldAddedInCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('main_categories',function(Blueprint $table){
            $table->string('main_category_tag','100');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('main_categories',function(Blueprint $table){
            $table->dropColumn('main_category_tag');
        });
    }
}

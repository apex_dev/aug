<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('banner_posts',function(Blueprint $table){
            $table->increments('id');
            $table->string('banner_post_header',150);
            $table->text('banner_post_body');
            $table->string('banner_post_images_1',255);
            $table->string('banner_post_images_2',255)->nullable();
            $table->string('banner_post_images_3',255)->nullable();
            $table->string('banner_post_images_4',255)->nullable();
            $table->string('banner_post_images_5',255)->nullable();
            $table->boolean('banner_post_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('banner_posts');
    }
}
